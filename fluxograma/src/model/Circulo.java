package model;

import java.awt.Graphics;
import java.awt.Graphics2D;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * 
 * @author lhries
 */
public class Circulo extends Figure {
	public Circulo(int x, int y) {
		super(x, y);
	}

	public Circulo(int x, int y, int width, int height) {
		super(x, y, width, height);
	}

	@Override
	public void desenha(Graphics g) {
		int x = (xIni <= xFim) ? xIni : xFim;
		int y = (yIni <= yFim) ? yIni : yFim;
		int larg = Math.abs(xIni - xFim);
		int alt = Math.abs(yIni - yFim);
		g.drawOval(x, y, larg, alt);
		if (this.estaSelecionado()) {
			g.drawOval(x - 2, y - 2, 4, 4);
			g.drawOval(x - 2, y + alt - 2, 4, 4);
			g.drawOval(x + larg - 2, y - 2, 4, 4);
			g.drawOval(x + larg - 2, y + alt - 2, 4, 4);
		}		
	}
	


	@Override
	public boolean intersecta(int x, int y) {
		if (x < this.getX())
			return false;
		if (x > (this.getX() + this.getLargura()))
			return false;
		if (y < this.getY())
			return false;
		if (y > (this.getY() + this.getAltura()))
			return false;
		return true;
	}
}
