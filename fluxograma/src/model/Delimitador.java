package model;

import java.awt.BasicStroke;
import java.awt.Graphics;
import java.awt.Graphics2D;

public class Delimitador extends Figure{

	private String text;
	
	public Delimitador(int x, int y, int width, int height, String text){
		super(x, y, width, height);
		this.setText(text);
	}
		
	public void setText(String text){
		this.text = text;
	}
	
	public String getText(){
		return this.text;
	}
	
	public void desenha(Graphics g) {
		Graphics2D g2d = (Graphics2D) g.create();		
		g2d.drawOval(this.xIni, this.yIni, this.getWidth(), this.getHeight());
		BasicStroke bs = new BasicStroke(5);
		g2d.setStroke(bs);		
		g2d.drawString(this.getText(), this.xIni + 25, this.yIni+ 25 );
		g2d.dispose();
	}

	@Override
	public boolean intersecta(int x, int y) {
		if (x < this.getX())
			return false;
		if (x > (this.getX() + this.getLargura()))
			return false;
		if (y < this.getY())
			return false;
		if (y > (this.getY() + this.getAltura()))
			return false;
		return true;
	}
	
	
	
	
}
