/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.awt.Color;
import java.awt.Graphics;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JPanel;

/**
 *
 * @author lhries
 */
class PainelDesenho extends JPanel {

    List<Figure> listaFiguras;

    public PainelDesenho() {
        listaFiguras = new ArrayList<>();
    }

    private void limparTela(Graphics g) {
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, this.getWidth(), this.getHeight());
        g.setColor(Color.BLACK);
    }

    public void addFigura(Figure f) {
        listaFiguras.add(f);
    }

    public Figure getUltimaFigura() {
        System.out.println(listaFiguras.size());
        return listaFiguras.get(listaFiguras.size() - 1);
    }

    public void removerFiguras() {
        listaFiguras.clear();
    }

    public void verificaSelecao(int x, int y) {
        deselecionarFiguras();
        for (Figure f : listaFiguras) {
            if (f.intersecta(x, y)) {
                deselecionarFiguras();
                f.selecionar();
            }
        }

    }

    public Figure getSelecionado() {
        for (Figure f : listaFiguras) {
            if (f.estaSelecionado()) {
                return f;
            }
        }
        return null;
    }

    public void deselecionarFiguras() {
        for (Figure f : listaFiguras) {
            f.deselecionar();
        }
        repaint();
    }

    @Override
    public void paintComponent(Graphics g) {
        limparTela(g);

        for (Figure f : listaFiguras) {
            f.desenha(g);
        }
    }
    
    public void carregaDados(File file) throws FileNotFoundException, IOException, ClassNotFoundException {
        FileInputStream fis = new FileInputStream(file);
        ObjectInputStream ois = new ObjectInputStream(fis);
        listaFiguras = (List<Figure>) ois.readObject();
        ois.close();
        fis.close();
    }

    public void salvarDados(File file) throws FileNotFoundException, IOException {
        FileOutputStream fos = new FileOutputStream(file);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.reset();
        oos.writeObject(listaFiguras);
        oos.close();
        fos.close();

    }           
}
