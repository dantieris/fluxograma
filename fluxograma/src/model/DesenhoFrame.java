/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;

import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;

/**
 * 
 * @author lhries
 */
public class DesenhoFrame extends JFrame {

	PainelDesenho painelDesenho;
	JToolBar barraFerramentas;
	JToggleButton botaoQuadrado, botaoLosango, botaoSelecionar, botaoInicio,
			botaoFim, botaoSubrotina, botaoSeta;
	JButton botaoLimpar;
	JPanel painelLateral;
	JList<String> listaFiguras;
	DefaultListModel<String> listaFigurasModel;

	public DesenhoFrame() {
		super("Meu Paint");

		iniciaComponentes();
		iniciaMenu();

		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setBounds(300, 150, 600, 400);
		setExtendedState(JFrame.MAXIMIZED_BOTH);
		setVisible(true);
	}
	private void iniciaComponentes() {		
		// Iniciando os botoes
		initButtons();
		
		// Iniciando a barra de ferramentas
		initToolbar();
		
		// Iniciando o painel principal de desenho
		initMainPanel();
		
		// Painel lateral
		initSidePanel();
		
		// Adicionando o painel principal e o lateral em um split panel.
		JSplitPane split = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, painelLateral, painelDesenho);
		
		this.add(split);
	}
	
	private void initSidePanel() {
		painelLateral = new JPanel();
		
		painelLateral.setPreferredSize(new Dimension(400, 400));
		
		listaFiguras = new JList<String>();
		
		listaFigurasModel = new DefaultListModel<String>();
		listaFigurasModel.addElement("Fluxograma 1");
		
		listaFiguras.setModel(listaFigurasModel);
		
		painelLateral.add(listaFiguras);
	}

	private void initMainPanel() {
		painelDesenho = new PainelDesenho();
		
		painelDesenho.add(barraFerramentas, BorderLayout.NORTH);
	}

	private void initButtons() {
		ButtonGroup grupo = new ButtonGroup();
		
		ImageIcon icone = null;
		
		icone = new ImageIcon("src/fluxograma/graphics/selecionar.png", "selecionar");
		botaoSelecionar = new JToggleButton(icone);
		grupo.add(botaoSelecionar);
		
		icone = new ImageIcon("src/fluxograma/graphics/inicio.png");
		botaoInicio = new JToggleButton(icone);
		grupo.add(botaoInicio);
		
		icone = new ImageIcon("src/fluxograma/graphics/fim.png");
		botaoFim = new JToggleButton(icone);
		grupo.add(botaoFim);
		
		icone = new ImageIcon("src/fluxograma/graphics/quadrado.png");
		botaoQuadrado = new JToggleButton(icone);
		grupo.add(botaoQuadrado);
		
		icone = new ImageIcon("src/fluxograma/graphics/losango.png");
		botaoLosango = new JToggleButton(icone);
		grupo.add(botaoLosango);

		icone = new ImageIcon("src/fluxograma/graphics/subrotina.png");
		botaoSubrotina = new JToggleButton(icone);
		grupo.add(botaoSubrotina);
		
		icone = new ImageIcon("src/fluxograma/graphics/seta.png");
		botaoSeta = new JToggleButton(icone);
		grupo.add(botaoSeta);
	}

	private void initToolbar() {
		barraFerramentas = new JToolBar(JToolBar.HORIZONTAL);
		
		barraFerramentas.setFloatable(false);
		
		barraFerramentas.add(botaoInicio);
		barraFerramentas.add(botaoQuadrado);
		barraFerramentas.add(botaoFim);
		barraFerramentas.add(botaoLosango);
		barraFerramentas.add(botaoSubrotina);
		barraFerramentas.add(botaoSeta);
	}

	private void iniciaMenu() {
		JMenuBar menuBar = new JMenuBar();

		JMenu menuArquivo = new JMenu("Arquivo");

		JMenuItem menuItemNovo = new JMenuItem("Novo");
		menuArquivo.add(menuItemNovo);

		JMenuItem menuItemAbrir = new JMenuItem("Abrir");
		menuArquivo.add(menuItemAbrir);

		JMenuItem menuItemSalvar = new JMenuItem("Salvar");
		menuArquivo.add(menuItemSalvar);

		JMenuItem menuItemSair = new JMenuItem("Sair");
		menuArquivo.add(menuItemSair);

		menuBar.add(menuArquivo);
		setJMenuBar(menuBar);

	}

	private class TrataBotoes implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// Adicionada a opção de desselecionar
			painelDesenho.deselecionarFiguras();

			if (e.getSource().equals(botaoLimpar)) {
				painelDesenho.removerFiguras();
			}
			painelDesenho.repaint();
		}
	}

	private class TrataMousePainel extends MouseAdapter {

		public void mouseReleased(MouseEvent e) {
			if (botaoInicio.isSelected()) {
				painelDesenho.addFigura(new Delimitador(e.getX(), e.getY(), 80,
						40, "Inicio"));
			}else if (botaoFim.isSelected()) {
				painelDesenho.addFigura(new Delimitador(e.getX(), e.getY(), 80,
						40, "Fim"));
			}
			painelDesenho.repaint();
		}

		@Override
		public void mousePressed(MouseEvent e) {
			if (botaoSelecionar.isSelected()) {
				painelDesenho.verificaSelecao(e.getX(), e.getY());
			} else if (botaoLosango.isSelected()) {
				Circulo circulo = new Circulo(e.getX(), e.getY());
				painelDesenho.addFigura(circulo);
			} else if (botaoQuadrado.isSelected()) {
				Quadrado quadrado = new Quadrado(e.getX(), e.getY());
				painelDesenho.addFigura(quadrado);
			}
			painelDesenho.repaint();
		}

		@Override
		public void mouseDragged(MouseEvent e) {
			if (botaoLosango.isSelected() || botaoQuadrado.isSelected()) {
				Figure figura = painelDesenho.getUltimaFigura();
				figura.setPontoFinal(e.getX(), e.getY());

			} else if (botaoSelecionar.isSelected()) {

				Figure figura = painelDesenho.getSelecionado();
				if (figura != null && figura.intersecta(e.getX(), e.getY())) {
					figura.moveTo(e.getX(), e.getY());
				}
			}

			painelDesenho.repaint();
		}
	}

	private class TrataMenu implements ActionListener {
		public static final String NOVO = "Novo";
		public static final String ABRIR = "Abrir";
		public static final String SALVAR = "Salvar";
		public static final String SAIR = "Sair";

		@Override
		public void actionPerformed(ActionEvent e) {
			JComponent componente = (JComponent) e.getSource();
			JFileChooser fileChooser = new JFileChooser();
			fileChooser
					.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
			switch (componente.getName()) {
			case NOVO:
				painelDesenho.removerFiguras();
				painelDesenho.repaint();
				break;
			case ABRIR:

				int returnVa = fileChooser.showOpenDialog(DesenhoFrame.this);

				if (returnVa == JFileChooser.APPROVE_OPTION) {
					File file = fileChooser.getSelectedFile();
					try {
						painelDesenho.carregaDados(file);
						painelDesenho.repaint();
						JOptionPane.showMessageDialog(DesenhoFrame.this,
								"Arquivo carregado com sucesso!");
					} catch (Exception exc) {
						JOptionPane.showMessageDialog(DesenhoFrame.this,
								"Erro ao ler do arquivo");
					}
				}
				break;
			case SALVAR:
				int returnVal = fileChooser.showSaveDialog(DesenhoFrame.this);
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					File file = fileChooser.getSelectedFile();
					try {
						painelDesenho.salvarDados(file);
					} catch (IOException ex) {
						JOptionPane.showMessageDialog(DesenhoFrame.this,
								"Erro ao salvar no arquivo");
					}
				}
				break;
			case SAIR:
				System.exit(0);
				break;
			}
		}
	}
}
