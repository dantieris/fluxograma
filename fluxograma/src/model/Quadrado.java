/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

import java.awt.Graphics;

/**
 * 
 * @author lhries
 */
public class Quadrado extends Figure {
	public Quadrado(int x, int y) {
		super(x, y);
	}

	public Quadrado(int x, int y, int width, int height) {
		super(x, y, width, height);
	}

	@Override
	public void desenha(Graphics g) {
		int x = this.getX();
		int y = this.getY();
		int larg = this.getLargura();
		int alt = this.getAltura();
		g.drawRect(x, y, larg, alt);

		if (this.estaSelecionado()) {
			g.drawOval(x - 2, y - 2, 4, 4);
			g.drawOval(x - 2, y + alt - 2, 4, 4);
			g.drawOval(x + larg - 2, y - 2, 4, 4);
			g.drawOval(x + larg - 2, y + alt - 2, 4, 4);
		}
	}
	
	public void draw(Graphics g){}

	@Override
	public boolean intersecta(int x, int y) {
		if (x < this.getX())
			return false;
		if (x > (this.getX() + this.getLargura()))
			return false;
		if (y < this.getY())
			return false;
		if (y > (this.getY() + this.getAltura()))
			return false;
		return true;
	}

}
