package fluxograma.entidades;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Fluxograma implements Serializable{

	private Delimitador start;
	private Delimitador end;
	private List<Figure> figureList;
	private String name;

	public Fluxograma(String name) {
		this.name = name;
		this.figureList = new ArrayList<Figure>();
	}

	/**
	 * Adiciona uma figua a lista de figuras
	 * 
	 * @param toAdd
	 */
	public void addFigure(Figure toAdd) {
		if(toAdd != null)
			this.figureList.add(toAdd);
	}

	/**
	 * Adiciona o ponto inicial do fluxograma
	 * 
	 * @param start
	 */
	public void setStart(Delimitador start) {
		this.start = start;
	}

	/**
	 * Adiciona o ponto final do fluxograma
	 * 
	 * @param end
	 */
	public void setEnd(Delimitador end) {
		this.end = end;
	}

	
	public Delimitador getStart(){
		return this.start;
	}
	
	public Delimitador getEnd(){
		return this.end;
	}
	
	/**
	 * Retorna a lista de figuras
	 * 
	 * @return
	 */
	public List<Figure> getFigureList() {
		return this.figureList;
	}

	public void setFigureList(List<Figure> toAdd) {
		this.figureList = toAdd;
	}

	@Override
	public String toString() {
		return this.name;
	}
	
}
