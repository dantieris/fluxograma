package fluxograma.entidades;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * This class is just a container for a set of Fluxograms
 * 
 * @author Guilherme
 *
 */
public class Project implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5435476137392780874L;
	private ArrayList<Fluxograma> flowcharList;
	private String name;

	public Project() {
		this.name = "unnamed project";
		this.flowcharList = new ArrayList<Fluxograma>();

	}

	public Project(String name) {
		this.name = name;
		this.flowcharList = new ArrayList<Fluxograma>();
	}

	public List<Fluxograma> getFlowcharList() {
		return this.flowcharList;
	}

	public void addFlowchart(Fluxograma toAdd) {
		if (toAdd != null)
			this.flowcharList.add(toAdd);
	}

	public String getName() {
		return this.name;
	}

	@Override
	public String toString() {
		return name;
	}

}
