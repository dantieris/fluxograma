package fluxograma.entidades;

import java.awt.Graphics;
import java.awt.Graphics2D;

public class Line {
	
	private Graphics2D g2d;
	private Figure start;
	private Figure end;
	private String text;
	public Figure getStart() {
		return start;
	}
	public void setStart(Figure start) {
		this.start = start;
	}
	public Figure getEnd() {
		return end;
	}
	public void setEnd(Figure end) {
		this.end = end;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	
	public void desenha(Graphics g){
		Graphics2D g2d = (Graphics2D)g.create();
		g2d.drawLine(this.start.xIni, this.start.yIni, this.end.xFim, this.end.yFim);		
		g.dispose();
	}
	

	
	
}
