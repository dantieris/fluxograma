package fluxograma.entidades;

import java.awt.BasicStroke;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.security.InvalidParameterException;

import javax.swing.JOptionPane;


public class Quadrado extends Figure {
	
	private String text;
	
	public Quadrado(int x, int y) {
		super(x, y);		
	}

	public Quadrado(int x, int y, int width, int height) {
		super(x, y, width, height);
		this.getTextFromUser();
	}

	@Override
	public void desenha(Graphics g) {
		Graphics2D g2d = (Graphics2D) g.create();
		int x = this.getX();
		int y = this.getY();
		int larg = this.getLargura();
		int alt = this.getAltura();
		g.drawRect(x, y, larg, alt);
		BasicStroke bs = new BasicStroke(5);
		g2d.setStroke(bs);		
		try{
			String texto = this.getText();
			g2d.drawString(texto, this.xIni + 25, this.yIni+ 25 );
		}catch(InvalidParameterException ex){			
			JOptionPane.showMessageDialog(null, ex.getMessage(), "Texto invalido",JOptionPane.INFORMATION_MESSAGE,null);
		}
		

		if (this.estaSelecionado()) {
			g.drawOval(x - 2, y - 2, 4, 4);
			g.drawOval(x - 2, y + alt - 2, 4, 4);
			g.drawOval(x + larg - 2, y - 2, 4, 4);
			g.drawOval(x + larg - 2, y + alt - 2, 4, 4);
		}
	}
	
	public void draw(Graphics g){}

	@Override
	public boolean intersecta(int x, int y) {
		if (x < this.getX())
			return false;
		if (x > (this.getX() + this.getLargura()))
			return false;
		if (y < this.getY())
			return false;
		if (y > (this.getY() + this.getAltura()))
			return false;
		return true;
	}
	
	private void getTextFromUser(){
		this.text = JOptionPane.showInputDialog(this, "Texto da figura: ");		
	}
	
	public void setText(String text){
		this.text = text;
	}
	
	public String getText() throws InvalidParameterException{
		if(this.text == null || this.text.equals(null) || this.text == "")
			throw new InvalidParameterException("Text em branco");
		return this.text;
	}
	

}
