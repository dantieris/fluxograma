package fluxograma.entidades;

import java.awt.BasicStroke;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.security.InvalidParameterException;

import javax.swing.JOptionPane;

public class Losango extends Figure {
	
	private String text;
	
	public Losango(int x, int y) {
		super(x, y);
		this.getTextFromUser();
	}

	public Losango(int x, int y, int width, int height) {
		super(x, y, width, height);
	}

	@Override
	public void desenha(Graphics g) {
		Graphics2D g2d = (Graphics2D)g.create();
		//necessarios p/desenhar os pontos quando for selecionado
		/*
		int x = (xIni <= xFim) ? xIni : xFim;
		int y = (yIni <= yFim) ? yIni : yFim;
		int larg = Math.abs(xIni - xFim);
		int alt = Math.abs(yIni - yFim);
		*/
		//calcular onde deve ficar o losango
		
		g.drawPolygon(new Polygon());        
		
		int[] xpoints= {this.xIni,this.xIni+80,this.xIni+160,this.xIni+80, this.xIni};
        int[] ypoints= {this.yIni,this.yIni-30,this.yIni,this.yIni+30,this.yIni};
		
        g.drawPolygon(new Polygon(xpoints, ypoints, 5));
        
        
		try{			
			String texto = this.getText();
			BasicStroke bs = new BasicStroke(5);
			g2d.setStroke(bs);		
			g2d.drawString(texto, this.xIni + 40, this.yIni + 5);
		}catch(InvalidParameterException ex){			
			JOptionPane.showMessageDialog(null, ex.getMessage(), "Texto invalido",JOptionPane.INFORMATION_MESSAGE,null);
		}
        
        /*
        if (this.estaSelecionado()) {
			g.drawOval(x - 2, y - 2, 4, 4);
			g.drawOval(x - 2, y + alt - 2, 4, 4);
			g.drawOval(x + larg - 2, y - 2, 4, 4);
			g.drawOval(x + larg - 2, y + alt - 2, 4, 4);
		}
		*/		
	}
	

	private void getTextFromUser(){
		this.text = JOptionPane.showInputDialog(this, "Texto da figura: ");		
	}
	
	public void setText(String text){
		this.text = text;
	}
	
	public String getText() throws InvalidParameterException{
		if(this.text == null || this.text.equals(null) || this.text == "")
			throw new InvalidParameterException("Text em branco");
		return this.text;
	}	
	
	

	@Override
	public boolean intersecta(int x, int y) {
		if (x < this.getX())
			return false;
		if (x > (this.getX() + this.getLargura()))
			return false;
		if (y < this.getY())
			return false;
		if (y > (this.getY() + this.getAltura()))
			return false;
		return true;
	}
}
