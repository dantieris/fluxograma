package fluxograma.controller;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;

import fluxograma.entidades.Circulo;
import fluxograma.entidades.Quadrado;
import fluxograma.entidades.Delimitador;
import fluxograma.entidades.Figure;


class MainFrame_OLD extends JFrame {

	MainPanel painelDesenho;
	JToolBar barraFerramentas;
	JToggleButton botaoQuadrado, botaoLosango, botaoSelecionar, botaoInicio,
			botaoFim;
	JButton botaoLimpar;

	public MainFrame_OLD() {
		super("Meu Paint");

		iniciaComponentes();
		iniciaMenu();

		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setBounds(100, 100, 400, 400);
		setVisible(true);
	}

	private void iniciaComponentes() {
		painelDesenho = new MainPanel();
		painelDesenho.addMouseListener(new TrataMousePainel());
		painelDesenho.addMouseMotionListener(new TrataMousePainel());
		this.add(painelDesenho);

		//aqui
		this.initButtons();
		this.initToolbar();
		this.add(barraFerramentas, BorderLayout.NORTH);
	}

	/**
	 * Define as configuracoes da barra de ferramentas 
	 */
	private void initToolbar(){
		barraFerramentas = new JToolBar();		
		barraFerramentas.add(botaoQuadrado);		
		barraFerramentas.add(botaoLosango);
		barraFerramentas.add(botaoInicio);	
		barraFerramentas.add(botaoFim);		
		barraFerramentas.add(botaoLimpar);
	}
	
	/**
	 * Define as configura��es dos botoes
	 */
	private void initButtons(){
		ButtonGroup grupo = new ButtonGroup();
		
		botaoSelecionar = new JToggleButton("Selecionar");
		barraFerramentas.add(botaoSelecionar);
		grupo.add(botaoSelecionar);
		
		botaoQuadrado = new JToggleButton("Quadrado");
		botaoQuadrado.addActionListener(new TrataBotoes());
		grupo.add(botaoQuadrado);
		
		botaoLosango = new JToggleButton("Losango");
		botaoLosango.addActionListener(new TrataBotoes());		
		grupo.add(botaoLosango);
		
		botaoLimpar = new JButton("Limpar");
		botaoLimpar.addActionListener(new TrataBotoes());
		
		botaoInicio = new JToggleButton("Inicio");
		botaoInicio.addActionListener(new TrataBotoes());		
		grupo.add(botaoInicio);
		
		botaoFim = new JToggleButton("Fim");
		botaoFim.addActionListener(new TrataBotoes());		
		grupo.add(botaoFim);
	}
	
	private void iniciaMenu() {
		JMenuBar menuBar = new JMenuBar();

		JMenu menuArquivo = new JMenu("Arquivo");

		JMenuItem menuItemNovo = new JMenuItem(TrataMenu.NOVO);
		menuItemNovo.setName(TrataMenu.NOVO);
		menuItemNovo.addActionListener(new TrataMenu());
		menuArquivo.add(menuItemNovo);

		JMenuItem menuItemAbrir = new JMenuItem(TrataMenu.ABRIR);
		menuItemAbrir.setName(TrataMenu.ABRIR);
		menuItemAbrir.addActionListener(new TrataMenu());
		menuArquivo.add(menuItemAbrir);

		JMenuItem menuItemSalvar = new JMenuItem(TrataMenu.SALVAR);
		menuItemSalvar.setName(TrataMenu.SALVAR);
		menuItemSalvar.addActionListener(new TrataMenu());
		menuArquivo.add(menuItemSalvar);

		JMenuItem menuItemSair = new JMenuItem(TrataMenu.SAIR);
		menuItemSair.setName(TrataMenu.SAIR);
		menuItemSair.addActionListener(new TrataMenu());
		menuArquivo.add(menuItemSair);

		menuBar.add(menuArquivo);
		setJMenuBar(menuBar);

	}

	private class TrataBotoes implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// Adicionada a opção de desselecionar
			painelDesenho.deselecionarFiguras();

			if (e.getSource().equals(botaoLimpar)) {
				painelDesenho.removerFiguras();
			}
			painelDesenho.repaint();
		}
	}

	private class TrataMousePainel extends MouseAdapter {

		public void mouseReleased(MouseEvent e) {
			if (botaoInicio.isSelected()) {
				painelDesenho.addFigura(new Delimitador(e.getX(), e.getY(), 80,
						40, "Inicio"));
			} else if (botaoFim.isSelected()) {
				painelDesenho.addFigura(new Delimitador(e.getX(), e.getY(), 80,
						40, "Fim"));
			}
			painelDesenho.repaint();
		}

		@Override
		public void mousePressed(MouseEvent e) {
			if (botaoSelecionar.isSelected()) {
				painelDesenho.verificaSelecao(e.getX(), e.getY());
			} else if (botaoLosango.isSelected()) {
				Circulo circulo = new Circulo(e.getX(), e.getY());
				painelDesenho.addFigura(circulo);
			} else if (botaoQuadrado.isSelected()) {
				Quadrado quadrado = new Quadrado(e.getX(), e.getY());
				painelDesenho.addFigura(quadrado);
			}
			painelDesenho.repaint();
		}

		@Override
		public void mouseDragged(MouseEvent e) {
			if (botaoLosango.isSelected() || botaoQuadrado.isSelected()) {
				Figure figura = painelDesenho.getUltimaFigura();
				figura.setPontoFinal(e.getX(), e.getY());

			} else if (botaoSelecionar.isSelected()) {

				Figure figura = painelDesenho.getSelecionado();
				if (figura != null && figura.intersecta(e.getX(), e.getY())) {
					figura.moveTo(e.getX(), e.getY());
				}
			}

			painelDesenho.repaint();
		}
	}

	private class TrataMenu implements ActionListener {
		public static final String NOVO = "Novo";
		public static final String ABRIR = "Abrir";
		public static final String SALVAR = "Salvar";
		public static final String SAIR = "Sair";

		@Override
		public void actionPerformed(ActionEvent e) {
			JComponent componente = (JComponent) e.getSource();
			JFileChooser fileChooser = new JFileChooser();
			fileChooser
					.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
			switch (componente.getName()) {
			case NOVO:
				painelDesenho.removerFiguras();
				painelDesenho.repaint();
				break;
			case ABRIR:

				int returnVa = fileChooser.showOpenDialog(MainFrame_OLD.this);

				if (returnVa == JFileChooser.APPROVE_OPTION) {
					File file = fileChooser.getSelectedFile();
					try {
						painelDesenho.carregaDados(file);
						painelDesenho.repaint();
						JOptionPane.showMessageDialog(MainFrame_OLD.this,
								"Arquivo carregado com sucesso!");
					} catch (Exception exc) {
						JOptionPane.showMessageDialog(MainFrame_OLD.this,
								"Erro ao ler do arquivo");
					}
				}
				break;
			case SALVAR:
				int returnVal = fileChooser.showSaveDialog(MainFrame_OLD.this);
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					File file = fileChooser.getSelectedFile();
					try {
						painelDesenho.salvarDados(file);
					} catch (IOException ex) {
						JOptionPane.showMessageDialog(MainFrame_OLD.this,
								"Erro ao salvar no arquivo");
					}
				}
				break;
			case SAIR:
				System.exit(0);
				break;
			}
		}
	}
}
