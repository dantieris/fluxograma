package fluxograma.controller;

import java.awt.Color;
import java.awt.Graphics;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.List;

import javax.swing.JPanel;

import fluxograma.entidades.Figure;
import fluxograma.entidades.Fluxograma;

/**
 * 
 * @author lhries
 */
public class MainPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 28886127615589151L;
	private Fluxograma fluxograma;
	List<Figure> listaFiguras;

	
	public MainPanel(boolean flux){
		
	}
	
	public MainPanel() {		
		fluxograma = new Fluxograma("Novo Fluxograma");
	}
	
	public MainPanel(String name){
		fluxograma = new Fluxograma(name);
	}

	private void limparTela(Graphics g) {
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, this.getWidth(), this.getHeight());
		g.setColor(Color.BLACK);
	}

	/**
	 * Adiciona uma figura a lista do fluxograma
	 * 
	 * @param f
	 */
	public void addFigura(Figure f) {
		// listaFiguras.add(f);		
		if(this.fluxograma != null){
			fluxograma.addFigure(f);
			System.out.println(fluxograma.toString());
		}
	}

	public Figure getUltimaFigura() {
		// System.out.println(listaFiguras.size());
		int size = this.fluxograma.getFigureList().size();
		System.out.println(size);
		return this.fluxograma.getFigureList().get(size - 1);
		// return listaFiguras.get(listaFiguras.size() - 1);
	}

	public void removerFiguras() {
		// listaFiguras.clear();
		if(this.fluxograma != null)
			this.fluxograma.getFigureList().clear();
	}

	public void verificaSelecao(int x, int y) {
		deselecionarFiguras();
		/*
		 * for (Figure f : listaFiguras) { if (f.intersecta(x, y)) {
		 * deselecionarFiguras(); f.selecionar(); } }
		 */
		for (Figure f : this.fluxograma.getFigureList()) {
			if (f.intersecta(x, y)) {
				deselecionarFiguras();
				f.selecionar();
			}
		}

	}

	public Figure getSelecionado() {
		/*
		 * for (Figure f : listaFiguras) { if (f.estaSelecionado()) { return f;
		 * } }
		 */
		for (Figure f : this.fluxograma.getFigureList())
			if (f.estaSelecionado())
				return f;

		return null;
	}

	public void deselecionarFiguras() {
		/*
		 * for (Figure f : listaFiguras) { f.deselecionar(); }
		 */
		if(this.fluxograma != null)
			if (fluxograma.getFigureList() != null)
				for (Figure f : this.fluxograma.getFigureList())
					f.deselecionar();

		repaint();
	}

	@Override
	public void paintComponent(Graphics g) {
		limparTela(g);

		/*
		 * for (Figure f : listaFiguras) { f.desenha(g); }
		 */
		if(this.fluxograma != null)
			if (fluxograma.getFigureList() != null)
				for (Figure f : this.fluxograma.getFigureList())
					f.desenha(g);
	}

	public void carregaDados(File file) throws FileNotFoundException,
			IOException, ClassNotFoundException {
		FileInputStream fis = new FileInputStream(file);
		ObjectInputStream ois = new ObjectInputStream(fis);
		// listaFiguras = (List<Figure>) ois.readObject();
		fluxograma.setFigureList((List<Figure>) ois.readObject());
		ois.close();
		fis.close();
	}

	public void salvarDados(File file) throws FileNotFoundException,
			IOException {
		FileOutputStream fos = new FileOutputStream(file);
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		oos.reset();
		// oos.writeObject(listaFiguras);
		oos.writeObject(this.fluxograma.getFigureList());
		oos.close();
		fos.close();
	}
	
	public Fluxograma getFluxograma(){
		return this.fluxograma;
	}
	
	public void addFluxogram(Fluxograma toAdd){
		if(toAdd != null)
			this.fluxograma = toAdd;
	}
	
	public void setFluxogram(String name){
		System.out.println("NOVO FLUX!");
		this.fluxograma = new Fluxograma(name);
	}
}
