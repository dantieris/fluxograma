package fluxograma.controller;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;

import fluxograma.entidades.Delimitador;
import fluxograma.entidades.Figure;
import fluxograma.entidades.Fluxograma;
import fluxograma.entidades.Losango;
import fluxograma.entidades.Project;
import fluxograma.entidades.Quadrado;

public class MainFrame extends JFrame {

	private static final long serialVersionUID = 1791980653142699469L;
	private MainPanel painelDesenho;
	private JToolBar barraFerramentas;
	private JToggleButton botaoQuadrado, botaoLosango, botaoSelecionar,
			botaoInicio, botaoFim, botaoSubrotina, botaoSeta;
	private JButton botaoLimpar;
	private JPanel painelLateral;
	private JSplitPane splitPane;
	private JList<Fluxograma> flowcharList;
	private DefaultListModel<Fluxograma> flowcharListModel;
	private ButtonGroup grupo;
	private Project project;
	private JLabel labelProjectName;

	public MainFrame() {
		super("Sistema de Fluxograma");

		initComponents();

		initMenu();

		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setBounds(300, 150, 600, 400);
		setExtendedState(JFrame.MAXIMIZED_BOTH);
		setVisible(true);
	}

	private void initComponents() {
		// Iniciando o painel principal de desenho
		initGambiarraPanel();
		//initMainPanel();

		// Iniciando os botoes
		initButtons();

		// Iniciando a barra de ferramentas
		initToolbar();

		// Painel lateral
		initSidePanel();

		// Adicionando o painel principal e o lateral em um split panel.
		initSplitPane();

		this.add(splitPane);

	}
	


	private void initSplitPane() {
		splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, painelLateral,
				painelDesenho);

		splitPane.setOneTouchExpandable(true);
		splitPane.setContinuousLayout(true);
	}

	private void initSidePanel() {
		painelLateral = new JPanel();
		painelLateral.setLayout(new BorderLayout());
		painelLateral.setPreferredSize(new Dimension(400, 400));
		painelLateral.setMinimumSize(new Dimension(250, 400));

		initList();

		labelProjectName = new JLabel("No Project Selected");

		painelLateral.add(labelProjectName, BorderLayout.NORTH);
		painelLateral.add(flowcharList, BorderLayout.CENTER);
	}

	private void initList() {
		flowcharList = new JList<Fluxograma>();

		flowcharListModel = new DefaultListModel<Fluxograma>();

		flowcharList.setModel(flowcharListModel);
	}

	private void initGambiarraPanel(){
		painelDesenho = new MainPanel(false);
		painelDesenho.addMouseListener(new TrataMousePainel());
		painelDesenho.addMouseMotionListener(new TrataMousePainel());
		this.add(painelDesenho);

	}
	
	private void initMainPanel() {
		painelDesenho = new MainPanel();
		painelDesenho.addMouseListener(new TrataMousePainel());
		painelDesenho.addMouseMotionListener(new TrataMousePainel());
		//this.add(painelDesenho);

	}

	private void initButtons() {

		ImageIcon icone = null;
		// ButtonGroup grupo = new ButtonGroup();
		this.grupo = new ButtonGroup();
		icone = new ImageIcon("src/fluxograma/graphics/selecionar.png",
				"selecionar");
		botaoSelecionar = new JToggleButton(icone);
		botaoSelecionar.addActionListener(new TrataBotoes());
		grupo.add(botaoSelecionar);

		icone = new ImageIcon("src/fluxograma/graphics/inicio.png");
		botaoInicio = new JToggleButton(icone);
		botaoInicio.addActionListener(new TrataBotoes());
		grupo.add(botaoInicio);

		icone = new ImageIcon("src/fluxograma/graphics/fim.png");
		botaoFim = new JToggleButton(icone);
		botaoFim.addActionListener(new TrataBotoes());
		grupo.add(botaoFim);

		icone = new ImageIcon("src/fluxograma/graphics/quadrado.png");
		botaoQuadrado = new JToggleButton(icone);
		botaoQuadrado.addActionListener(new TrataBotoes());
		grupo.add(botaoQuadrado);

		icone = new ImageIcon("src/fluxograma/graphics/losango.png");
		botaoLosango = new JToggleButton(icone);
		botaoLosango.addActionListener(new TrataBotoes());
		grupo.add(botaoLosango);

		icone = new ImageIcon("src/fluxograma/graphics/subrotina.png");
		botaoSubrotina = new JToggleButton(icone);
		botaoSubrotina.addActionListener(new TrataBotoes());
		grupo.add(botaoSubrotina);

		icone = new ImageIcon("src/fluxograma/graphics/seta.png");
		botaoSeta = new JToggleButton(icone);
		botaoSeta.addActionListener(new TrataBotoes());
		grupo.add(botaoSeta);

		icone = new ImageIcon("src/fluxograma/graphics/limpar.png");
		botaoLimpar = new JButton(icone);
		botaoLimpar.addActionListener(new TrataBotoes());
	}

	private void initToolbar() {
		barraFerramentas = new JToolBar(JToolBar.HORIZONTAL);

		barraFerramentas.setFloatable(false);

		barraFerramentas.add(botaoSelecionar);
		barraFerramentas.add(botaoInicio);
		barraFerramentas.add(botaoQuadrado);
		barraFerramentas.add(botaoFim);
		barraFerramentas.add(botaoLosango);
		barraFerramentas.add(botaoSubrotina);
		barraFerramentas.add(botaoSeta);
		barraFerramentas.add(botaoLimpar);

		painelDesenho.add(barraFerramentas, BorderLayout.NORTH);
	}

	private void initMenu() {
		JMenuBar menuBar = new JMenuBar();

		JMenu menuArquivo = new JMenu("File");

		initMenuArquivo(menuArquivo);

		JMenu menuEditar = new JMenu("Edit");

		initMenuEditar(menuEditar);

		menuBar.add(menuArquivo);
		menuBar.add(menuEditar);
		setJMenuBar(menuBar);

	}

	private void initMenuEditar(JMenu menuEditar) {
		JMenuItem menuItemRenomear = new JMenuItem(TrataMenu.RENAME);
		menuItemRenomear.setName(TrataMenu.RENAME);
		menuItemRenomear.addActionListener(new TrataMenu());
		menuEditar.add(menuItemRenomear);

		JMenuItem menuItemDeletar = new JMenuItem(TrataMenu.DELETE);
		menuItemDeletar.setName(TrataMenu.DELETE);
		menuItemDeletar.addActionListener(new TrataMenu());
		menuEditar.add(menuItemDeletar);
	}

	private void initMenuArquivo(JMenu menuArquivo) {
		JMenuItem menuItemNovoProjeto = new JMenuItem(TrataMenu.NEW_PROJECT);
		menuItemNovoProjeto.setName(TrataMenu.NEW_PROJECT);
		menuItemNovoProjeto.addActionListener(new TrataMenu());
		menuArquivo.add(menuItemNovoProjeto);

		JMenuItem menuItemNovoFluxograma = new JMenuItem(
				TrataMenu.NEW_FLOWCHART);
		menuItemNovoFluxograma.setName(TrataMenu.NEW_FLOWCHART);
		menuItemNovoFluxograma.addActionListener(new TrataMenu());
		menuArquivo.add(menuItemNovoFluxograma);

		JMenuItem menuItemAbrir = new JMenuItem("Abrir");
		menuItemAbrir.setName(TrataMenu.OPEN);
		menuItemAbrir.addActionListener(new TrataMenu());
		menuArquivo.add(menuItemAbrir);

		JMenuItem menuItemSalvar = new JMenuItem("Salvar");
		menuItemSalvar.setName(TrataMenu.SAVE);
		menuItemSalvar.addActionListener(new TrataMenu());
		menuArquivo.add(menuItemSalvar);

		JMenuItem menuItemSair = new JMenuItem("Sair");
		menuItemSair.setName(TrataMenu.EXIT);
		menuItemSair.addActionListener(new TrataMenu());
		menuArquivo.add(menuItemSair);
	}

	/**
	 * Carrega o projeto de um arquivo.
	 * 
	 * @param file
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public void loadProject(File file) throws FileNotFoundException,
			IOException, ClassNotFoundException {
		FileInputStream fis = new FileInputStream(file);
		ObjectInputStream ois = new ObjectInputStream(fis);

		project = (Project) ois.readObject();

		loadFlowcharProject();

		fis.close();
		ois.close();
	}

	/**
	 * Carrega os fluxogramas do projeto para o modelo da jlist.
	 */
	private void loadFlowcharProject() {
		if (project != null && !project.getFlowcharList().isEmpty()) {
			flowcharListModel.clear();

			for (Fluxograma f : project.getFlowcharList())
				flowcharListModel.addElement(f);
		}

		painelDesenho.repaint();
	}

	private void saveProject(File file) throws FileNotFoundException,
			IOException {
		FileOutputStream fos = new FileOutputStream(file);
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		oos.reset();

		oos.writeObject(this.project);

		oos.close();
		fos.close();
	}

	public void addFlowchar() {
		String flowchartName = JOptionPane.showInputDialog(MainFrame.this,
				"Enter the flowchart name: ");

		if (flowchartName.length() <= 0)
			flowchartName = "Unamed Flowchart";

		Fluxograma newFlowchart = new Fluxograma(flowchartName);
		//mudei aqui
		initMainPanel();
		painelDesenho.setFluxogram(flowchartName);		
		//
		project.addFlowchart(newFlowchart);
		flowcharListModel.addElement(newFlowchart);
		painelDesenho.repaint();
	}

	private class TrataBotoes implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// Adicionada a opcao de desselecionar
			painelDesenho.deselecionarFiguras();
			if (e.getSource().equals(botaoLimpar)) {
				painelDesenho.removerFiguras();
				painelDesenho.getFluxograma().setStart(null);
				painelDesenho.getFluxograma().setEnd(null);
			}
			painelDesenho.repaint();
		}
	}

	private class TrataMousePainel extends MouseAdapter {

		public void mouseClicked(MouseEvent e) {
			if (botaoLosango.isSelected()) {
				Losango losango = new Losango(e.getX(), e.getY());
				painelDesenho.addFigura(losango);
			} else if (botaoQuadrado.isSelected()) {
				Quadrado quadrado = new Quadrado(e.getX(), e.getY(), 160, 50);
				painelDesenho.addFigura(quadrado);
			}
			painelDesenho.repaint();
		}

		public void mouseReleased(MouseEvent e) {
			// Se for inicio ou fim de fluxo
			if (botaoInicio.isSelected()) {
				if (painelDesenho.getFluxograma().getStart() == null) {
					painelDesenho.getFluxograma().setStart(
							new Delimitador(e.getX(), e.getY(), 100, 50,
									"Inicio"));
					painelDesenho.addFigura(painelDesenho.getFluxograma()
							.getStart());
				}
			} else if (botaoFim.isSelected()) {
				if (painelDesenho.getFluxograma().getEnd() == null) {
					painelDesenho.getFluxograma()
							.setEnd(new Delimitador(e.getX(), e.getY(), 100,
									50, "Fim"));
					painelDesenho.addFigura(painelDesenho.getFluxograma()
							.getEnd());
				}
			}
			// se for uma seta
			if (botaoSeta.isSelected()) {
				// o primeiro click � o inico, o segundo � o fim

			}

			painelDesenho.repaint();
		}

		@Override
		public void mousePressed(MouseEvent e) {
			if (botaoSelecionar.isSelected()) {
				painelDesenho.verificaSelecao(e.getX(), e.getY());
			} /*
			 * else if (botaoLosango.isSelected()) { Circulo circulo = new
			 * Circulo(e.getX(), e.getY()); painelDesenho.addFigura(circulo); }
			 * else if (botaoQuadrado.isSelected()) { Quadrado quadrado = new
			 * Quadrado(e.getX(), e.getY()); painelDesenho.addFigura(quadrado);
			 * }
			 */
			painelDesenho.repaint();

		}

		@Override
		public void mouseDragged(MouseEvent e) {
			/*
			 * if (botaoLosango.isSelected() || botaoQuadrado.isSelected()) {
			 * Figure figura = painelDesenho.getUltimaFigura();
			 * figura.setPontoFinal(e.getX(), e.getY());
			 * 
			 * } else if
			 */
			if (botaoSelecionar.isSelected()) {

				Figure figura = painelDesenho.getSelecionado();
				if (figura != null && figura.intersecta(e.getX(), e.getY())) {
					figura.moveTo(e.getX(), e.getY());
				}
			}

			painelDesenho.repaint();
		}
	}

	private class TrataMenu implements ActionListener {
		// Itens do menu arquivo.

		public static final String NEW_PROJECT = "New Project";
		public static final String NEW_FLOWCHART = "New Flowchart";
		public static final String OPEN = "Open";
		public static final String SAVE = "Save";
		public static final String EXIT = "Exit";

		// Itens do menu editar.
		public static final String RENAME = "Rename";
		public static final String DELETE = "Delete";

		@Override
		public void actionPerformed(ActionEvent e) {
			JComponent componente = (JComponent) e.getSource();
			JFileChooser fileChooser = new JFileChooser();
			fileChooser
					.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);

			switch (componente.getName()) {
			case RENAME:
				Figure figura = painelDesenho.getSelecionado();

				if (figura != null && figura instanceof Quadrado) {
					Quadrado square = (Quadrado) figura;

					String text = JOptionPane.showInputDialog(MainFrame.this,
							"Enter the new text: ");

					square.setText(text);
					painelDesenho.repaint();
				}
				break;
			case DELETE:

				break;
			case NEW_PROJECT:
				// Recebendo o nome do projeto via option pane.
				String projectName = JOptionPane.showInputDialog(
						MainFrame.this, "Enter the project name: ");

				// Colocando um nome padrao para o projeto, caso seja nulo.
				if (projectName.length() <= 0)
					projectName = "Unnamed Project";

				project = new Project(projectName);

				// Atualizando o label com o nome do projeto.
				MainFrame.this.labelProjectName.setText(project.getName());
				break;
			case NEW_FLOWCHART:
				if (project != null) {					
					addFlowchar();					
				}
				break;
			case OPEN:

				int returnVa = fileChooser.showOpenDialog(MainFrame.this);

				if (returnVa == JFileChooser.APPROVE_OPTION) {
					File file = fileChooser.getSelectedFile();
					try {
						loadProject(file);
						JOptionPane.showMessageDialog(MainFrame.this,
								"Arquivo carregado com sucesso!");
					} catch (Exception exc) {
						JOptionPane.showMessageDialog(MainFrame.this,
								"Erro ao ler do arquivo");
					}
				}
				break;
			case SAVE:
				int returnVal = fileChooser.showSaveDialog(MainFrame.this);
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					File file = fileChooser.getSelectedFile();
					try {
						saveProject(file);

					} catch (IOException ex) {
						JOptionPane.showMessageDialog(MainFrame.this,
								"Erro ao salvar no arquivo");
					}
				}
				break;
			case EXIT:
				System.exit(0);
				break;
			}
		}
	}

}
